package org.arecap.arhaic.edge.i18nbean;

import java.io.Serializable;

public interface I18nPropertyFactory<ID extends Serializable> extends HasIdentity<ID> {

    ID getI18nId();

}
