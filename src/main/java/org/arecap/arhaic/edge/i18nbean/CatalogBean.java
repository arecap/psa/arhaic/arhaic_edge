package org.arecap.arhaic.edge.i18nbean;

import java.io.Serializable;

public interface CatalogBean<ID extends Serializable> extends I18nBean<ID> {

    String getLabel();

    void setLabel(String label);

    String getDetails();

    void setDetails(String details);


}
