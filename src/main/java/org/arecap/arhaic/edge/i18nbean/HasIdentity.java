package org.arecap.arhaic.edge.i18nbean;

import java.io.Serializable;

public interface HasIdentity<ID extends Serializable> extends Serializable {

    ID getId();

}
