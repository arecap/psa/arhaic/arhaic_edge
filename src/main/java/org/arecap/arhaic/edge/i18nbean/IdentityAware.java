package org.arecap.arhaic.edge.i18nbean;

import java.io.Serializable;

public interface IdentityAware<ID extends Serializable> extends Serializable {

    void setId(ID id);

}
