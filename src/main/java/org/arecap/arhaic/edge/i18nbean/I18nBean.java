package org.arecap.arhaic.edge.i18nbean;

import java.io.Serializable;

public interface I18nBean<ID extends Serializable>
        extends I18nPropertyFactory<ID>, ConfigurableI18n<ID> {
}
