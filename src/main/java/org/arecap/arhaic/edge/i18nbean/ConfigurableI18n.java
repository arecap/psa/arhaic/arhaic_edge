package org.arecap.arhaic.edge.i18nbean;

import java.io.Serializable;

public interface ConfigurableI18n<ID extends Serializable> extends IdentityAware<ID> {

    void setI18nId(ID i18nId);

}
