package org.arecap.arhaic.edge.arhaic.route.home;

import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.arecap.arhaic.edge.mvp.DefaultFlowPresenter;

@SpringComponent
@UIScope
public class HomeDashboardPresenter extends DefaultFlowPresenter<HomeDashboardView> {
}
