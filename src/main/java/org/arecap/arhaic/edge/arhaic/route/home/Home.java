package org.arecap.arhaic.edge.arhaic.route.home;

import com.vaadin.flow.router.Route;
import org.arecap.arhaic.edge.arhaic.route.template.RouteHorizontalLayout;
import org.springframework.beans.factory.annotation.Autowired;

@Route("home")
public class Home extends RouteHorizontalLayout {

    @Autowired
    private HomeDashboardView dashboardView;


    @Override
    protected void buildLayout() {
        addContentView(dashboardView);
    }
}
