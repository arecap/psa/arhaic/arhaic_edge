package org.arecap.arhaic.edge.arhaic.route.dmz.edgefiles;

import org.arecap.arhaic.edge.service.ListRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EdgeFileRepository extends ListRepository<EdgeFile, Long> {
}
