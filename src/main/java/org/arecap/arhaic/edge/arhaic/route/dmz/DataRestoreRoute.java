package org.arecap.arhaic.edge.arhaic.route.dmz;

import com.vaadin.flow.component.Text;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.WildcardParameter;
import org.arecap.arhaic.edge.arhaic.route.dmz.edgefiles.EdgeFilesView;
import org.arecap.arhaic.edge.arhaic.route.template.RouteHorizontalLayout;
import org.springframework.beans.factory.annotation.Autowired;

@Route("dmz/php_edge/dbrestore")
public class DataRestoreRoute extends RouteHorizontalLayout implements HasUrlParameter<String> {


    @Autowired
    private EdgeFilesView edgeFilesView;

    @Override
    protected void buildLayout() {
        add(edgeFilesView);
    }

    @Override
    public void setParameter(BeforeEvent event, @WildcardParameter String parameter) {
        if (parameter.isEmpty()) {
            add(new Text("Welcome anonymous."));
        } else {
            add(new Text(String.format("Handling parameter %s.", parameter)));
        }
    }
}
