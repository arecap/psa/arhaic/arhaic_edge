package org.arecap.arhaic.edge.arhaic.boot;


import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;

@Configuration
@ConfigurationProperties("arhaic.global")
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "entityManagerFactoryArhaicGlobal",
        transactionManagerRef = "transactionManagerArhaicGlobal",
        basePackages = {"org.arecap.arhaic.edge.arhaic.arhaicglobal.repository"}
)
public class BootDataSourceArhaicGlobal extends HikariConfig {

    public final static String PERSISTENCE_UNIT_NAME = "arhaicGlobal";

    public final static String MODEL_PACKAGE = "org.arecap.arhaic.edge.arhaic.arhaicglobal.entities";

    @Bean
    public HikariDataSource dataSourceArhaicGlobal() {
        return new HikariDataSource(this);
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryArhaicGlobal(final HikariDataSource dataSourceWrite) {
        return new LocalContainerEntityManagerFactoryBean() {{
            setDataSource(dataSourceWrite);
            setPersistenceProviderClass(HibernatePersistenceProvider.class);
            setPersistenceUnitName(PERSISTENCE_UNIT_NAME);
            setPackagesToScan(MODEL_PACKAGE);
        }};
    }

    @Bean
    public PlatformTransactionManager transactionManagerArhaicGlobal(EntityManagerFactory entityManagerFactoryArhaicGlobal) {
        return new JpaTransactionManager(entityManagerFactoryArhaicGlobal);
    }

//    @Bean(initMethod = "migrate")
//    public Flyway flywayArhaicGlobal() throws SQLException {
//        Flyway flyway = new Flyway();
//        flyway.setBaselineOnMigrate(true);
//        flyway.setLocations("classpath:db/migration/applicationsettings");
//        flyway.setSqlMigrationPrefix("V");
//        flyway.setEncoding("UTF-8");
//        flyway.setValidateOnMigrate(false);
//        flyway.setOutOfOrder(true);
//        flyway.setDataSource(dataSourceArhaicGlobal());
//        return flyway;
//    }
//MySql 5.1 not supported!!!


}
