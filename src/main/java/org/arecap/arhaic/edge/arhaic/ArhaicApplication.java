package org.arecap.arhaic.edge.arhaic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.vaadin.spring.events.annotation.EnableEventBus;

@SpringBootApplication
@ComponentScan(basePackages = "org.arecap.arhaic.edge")
//@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class,HibernateJpaAutoConfiguration.class})
@EnableEventBus
public class ArhaicApplication {

    public static void main(String[] args) {
        SpringApplication.run(ArhaicApplication.class, args);
    }

}
