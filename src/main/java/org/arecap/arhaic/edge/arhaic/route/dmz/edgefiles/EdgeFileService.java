package org.arecap.arhaic.edge.arhaic.route.dmz.edgefiles;

import org.arecap.arhaic.edge.i18nbean.I18nRepositoryService;
import org.arecap.arhaic.edge.service.ListRepository;
import org.arecap.arhaic.edge.service.RepositoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EdgeFileService implements RepositoryService<EdgeFile, Long>, I18nRepositoryService<EdgeFile, Long> {

    @Autowired
    private EdgeFileRepository edgeFileRepository;

    @Override
    public ListRepository<EdgeFile, Long> getRepository() {
        return edgeFileRepository;
    }

}
