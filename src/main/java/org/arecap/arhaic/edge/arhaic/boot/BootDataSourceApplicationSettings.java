package org.arecap.arhaic.edge.arhaic.boot;


import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.flywaydb.core.Flyway;
import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import java.sql.SQLException;

@Configuration
@ConfigurationProperties("application.settings")
@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "entityManagerFactoryApplicationSettings",
        transactionManagerRef = "transactionManagerApplicationSettings",
        basePackages = {"org.arecap.arhaic.edge.arhaic.settings.repository"}
)
public class BootDataSourceApplicationSettings extends HikariConfig {

    public final static String PERSISTENCE_UNIT_NAME = "applicationSettings";

    public final static String MODEL_PACKAGE = "org.arecap.arhaic.edge.arhaic.settings.entities";


    @Bean
    @Primary
    public HikariDataSource dataSourceApplicationSettings() {
        return new HikariDataSource(this);
    }

    @Bean
    @Primary
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryApplicationSettings(final HikariDataSource dataSourceWrite) {
        return new LocalContainerEntityManagerFactoryBean() {{
            setDataSource(dataSourceWrite);
            setPersistenceProviderClass(HibernatePersistenceProvider.class);
            setPersistenceUnitName(PERSISTENCE_UNIT_NAME);
            setPackagesToScan(MODEL_PACKAGE);
        }};
    }

    @Bean
    @Primary
    public PlatformTransactionManager transactionManagerApplicationSettings(EntityManagerFactory entityManagerFactoryApplicationSettings) {
        return new JpaTransactionManager(entityManagerFactoryApplicationSettings);
    }



    @Bean(initMethod = "migrate")
    @Primary
    public Flyway flywayApplicationSettings() throws SQLException {
        Flyway flyway = new Flyway();
        flyway.setBaselineOnMigrate(true);
        flyway.setLocations("classpath:db/migration/applicationsettings");
        flyway.setSqlMigrationPrefix("V");
        flyway.setEncoding("UTF-8");
        flyway.setValidateOnMigrate(false);
        flyway.setOutOfOrder(true);
        flyway.setDataSource(dataSourceApplicationSettings());
        return flyway;
    }

}
