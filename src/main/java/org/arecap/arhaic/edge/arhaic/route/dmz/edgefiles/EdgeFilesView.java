package org.arecap.arhaic.edge.arhaic.route.dmz.edgefiles;

import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.arecap.arhaic.edge.mvp.component.verticallayout.VerticalLayoutCrudFlowEntityView;

import java.util.Collection;

@SpringComponent
@UIScope
public class EdgeFilesView extends VerticalLayoutCrudFlowEntityView<EdgeFilesPresenter, EdgeFile> {

    @Override
    public void setItems(Collection<EdgeFile> collection) {

    }

}
