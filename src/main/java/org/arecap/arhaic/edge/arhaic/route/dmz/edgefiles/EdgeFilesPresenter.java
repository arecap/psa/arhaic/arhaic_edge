package org.arecap.arhaic.edge.arhaic.route.dmz.edgefiles;

import com.vaadin.flow.spring.annotation.SpringComponent;
import com.vaadin.flow.spring.annotation.UIScope;
import org.arecap.arhaic.edge.mvp.DefaultFlowEntityPresenter;

@SpringComponent
@UIScope
public class EdgeFilesPresenter extends DefaultFlowEntityPresenter<EdgeFilesView, EdgeFile, Long, EdgeFileService> {
}
