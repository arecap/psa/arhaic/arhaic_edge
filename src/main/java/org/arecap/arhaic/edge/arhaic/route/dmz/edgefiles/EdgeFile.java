package org.arecap.arhaic.edge.arhaic.route.dmz.edgefiles;

import org.arecap.arhaic.edge.i18nbean.CatalogBean;
import org.hibernate.envers.Audited;

import javax.persistence.*;

@Entity
@Audited
@Table(name = "edge_file")
public class EdgeFile implements CatalogBean<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "edge_file_id")
    private Long id;

    @Column
    private Long i18nId;

    @Column(columnDefinition = "text")
    private String label;

    @Column(columnDefinition = "text")
    private String details;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Long getI18nId() {
        return i18nId;
    }

    @Override
    public void setI18nId(Long i18nId) {
        this.i18nId = i18nId;
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public String getDetails() {
        return details;
    }

    @Override
    public void setDetails(String details) {
        this.details = details;
    }

}
