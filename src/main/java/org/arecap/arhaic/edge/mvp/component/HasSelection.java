package org.arecap.arhaic.edge.mvp.component;

import java.util.Optional;

public interface HasSelection<T> {

    void publishSelection(Optional<T> selection);

}
