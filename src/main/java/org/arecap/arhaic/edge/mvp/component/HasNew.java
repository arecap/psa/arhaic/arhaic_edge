package org.arecap.arhaic.edge.mvp.component;

public interface HasNew<T> {

    void createItem();

}
