package org.arecap.arhaic.edge.mvp.component.horizontallayout;

import com.vaadin.flow.component.AttachEvent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import org.arecap.arhaic.edge.mvp.FlowPresenter;
import org.arecap.arhaic.edge.mvp.FlowView;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class HorizontalLayoutFlowView<P extends FlowPresenter> extends HorizontalLayout implements FlowView<P> {

    @Autowired
    private P presenter;

    @Override
    public void onAttach(AttachEvent attachEvent) {
        FlowView.super.onAttach(attachEvent);
    }

    @Override
    public P getPresenter() {
        return presenter;
    }

}
